#pragma once
#include <iostream>
using namespace std;
// ********* D�claration de la classe matrice ************
class vect;                    // pour pouvoir compiler correctement
class matrice
{
	double mat[3][3];         // matrice 3 X 3
public:
	matrice(double t[3][3])    // constructeur, � partir d�un tableau 3 x 3 
	{
		int i; int j;
		for (i = 0; i < 3; i++)
			for (j = 0; j < 3; j++)
				mat[i][j] = t[i][j];
	}
	vect prod(const vect &) const;  // prod = fonction membre (cette fois)
};


// ********* D�finition de la fonction prod ************

