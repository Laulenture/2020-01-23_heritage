#pragma once
#include <iostream>


using namespace std;
class matrice;        // pour pouvoir compiler la d�claration de vect

	   // *********** La classe vect *******************
class vect
{
	double v[3];      // vecteur � 3 composantes
public:
	vect(double v1 = 0, double v2 = 0, double v3 = 0)   // constructeur
	{
		v[0] = v1; v[1] = v2; v[2] = v3;
	}
	friend vect prod(const matrice&, const vect &);  // fct amie ind�pendante
	void affiche() const
	{
		int i;
		for (i = 0; i < 3; i++) cout << v[i] << " ";
		cout << "\n";
	}
};