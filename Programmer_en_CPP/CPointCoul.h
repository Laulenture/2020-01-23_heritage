#pragma once
#include "CPoint.h"

class CPointCoul : public CPoint
{
private:
	char *cCouleur;

public:
	//Assesseurs
	char* getCouleur();

	//Mutateurs
	void setCouleur(char* cCouleur);

	//Constructeurs
	CPointCoul();
	//CPointCoul(int nX, int nY, const char cColor[]);
};
