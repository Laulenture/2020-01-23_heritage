#include "CCercle.h"

#include <iostream>




CPoint & CCercle::operator[](int i)const
{
	
	if (i >= nPoint) {
		return adr[0];
	}
	return adr[i];
}

CCercle::CCercle(int nX, int nY,const char cColor[], int nR, int nElement):pt(nX,nY,"vert")
{
	this->nR = nR;
	adr = new CPoint[nPoint=nElement];

}


CCercle::~CCercle()
{ 
	delete[] adr;
}


std::ostream & operator<<(std::ostream & os, const CCercle &c)
{
	
	os <<"X :"<< c.pt.nX;
	os <<"Y: "<< c.pt.nY;
	os << "Rayon : "<<c.nR;
	os << "Couleur : "<<c.pt.cColor;

	return os;
}
