#pragma once
#include<iostream>
//Point dans un plan

class CCercle;//forward d�claration
typedef enum eCouleur {rouge,vert,bleur}eCouleur;




class CPoint
{

private:
	//Donn�es membres de la classe 
	int nX;
	int nY;
	char *cColor;
	eCouleur coul;


public :
	//Prototypes ou d�clarations

	//Assesseurs
	int getX();
	int getY();
	char * getColor();
	void setColor(char * cColor);

	//Mutateurs
	void setX(int nX);
	void setY(int nY);


	//Constructeur
	CPoint(int nX = 0, int nY = 0);
	CPoint(int nX, int nY,const char cColor[]);
	//Constructeur de copie
	CPoint(const CPoint &p);



	//retour par r�f�rence
	CPoint &deplacePointRef(const int x, const int y);

	//retour par adresse
	CPoint *deplacePointAdr(const int x, const int y);

	//Fonction ind�pendante amie d'une classe
	friend bool coincide(const CPoint &p, const CPoint &q);

	//Fonction membre de la classe Point amie de la classe Cercle
	void affichePointCercle(const CCercle &c);

	//Fonction amie de plusieurs classes
	friend void affichePointCercle(const CPoint &pt, const CCercle &c);
	
	/*Surcharge operateur 
	Fct amie ou membre
	Si un operateur doit absolument recevoir un type de base en premier argument,
	il ne peut pas �tre d�fini comme fonction membre
	*/

	//surcharge operateur affectation
	CPoint &operator=(const CPoint &pt1);

	//Surcharge operateur +
	//friend CPoint operator+(const CPoint &pt1, const CPoint &pt2);

	//CPoint operator+(const CPoint &pt)const;

	//Surcharge operateur ==
	friend bool operator==(const CPoint &pt1, const CPoint &pt2);

	//Surcharge Incrementation
	CPoint operator++(); //Notation prefixee

	//Surcharge Incrmentation
	CPoint operator++(int n); //Notation postfixee

	//Surcharge flux de sortie (cout)
	friend std::ostream& operator<<(std::ostream &os, const CPoint &pt);
	
	//Surcharge flux entr�e (cin) 
	friend std::istream& operator>>(std::istream &is,CPoint &pt);

	//Surcharge flux de sortie (cout) amie de CPoint
	friend std::ostream& operator<<(std::ostream &os, const CCercle &c);

	
	operator int()const;



	//Destructeur
	~CPoint();



	
};

