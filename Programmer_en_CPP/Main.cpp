
#include <iostream>
#include <cstdlib>
//#include "CPoint.h"
//#include "CCercle.h"
#include "CVect_Ind.h"
#include "CMatrice_Ind.h"
//#include "CVect_Membre.h"
//#include "CMatrice_Membre.h"





//POO C++
//Les fonctions amies
//fonction indépendante, amie d'une classe
//fonction membre d'une classe, amie d'une autre classe
//fonction amie de plusieurs classes

//Toutes les fonctions membres d'une classe, amie d'une autre classe
/*Pour dire que toutes les fonctions membres d'une classe B sont amies de la classe A,
on placera dans la classe A la déclaration :
friend class B;
*/



int main()
{
	


	//Programme test prod vect  fct Amie Independante
	vect w(1, 2, 3);
	vect res;
	double tb[3][3] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	matrice a = tb;
	res = prod(a, w);
	res.affiche();

	//Programme test prod vect  fct Amie Membre
	/*vect w(1, 2, 3);
	vect res;
	double tb[3][3] = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} };
	matrice a = tb;
	res = a.prod(w);
	res.affiche();*/

	


	
	system("pause");


	return 0;
}

