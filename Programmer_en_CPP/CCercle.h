#pragma once

#include "CPoint.h"



class CCercle
{
	CPoint pt;
	int nR;

	CPoint *adr;
	int nPoint;

public:
	
	//Fct amie et membre de la classe Point
	friend void CPoint::affichePointCercle(const CCercle &c);


	//Fonction amie de plusieurs classes
	friend void affichePointCercle(const CPoint &pt, const CCercle &c);

	//Surcharge flux de sortie (cout) amie de CPoint
	friend std::ostream& operator<<(std::ostream& os, const CCercle &c);

	CPoint &operator[](int)const;

	CCercle(int nX, int nY, const char cColor[], int nR,int nElement);
	~CCercle();
};

