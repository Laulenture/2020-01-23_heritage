#pragma once

class vect;
// *********** La classe matrice *****************
class matrice
{
	double mat[3][3];       // matrice 3 X 3
public:
	matrice(double t[3][3])    // constructeur, � partir d�un tableau 3 x 3 
	{
		int i; int j;
		for (i = 0; i < 3; i++)
			for (j = 0; j < 3; j++)
				mat[i][j] = t[i][j];
	}
	friend vect prod(const matrice &, const vect &);
};

// ********** La fonction prod *****************
vect prod(const matrice & m, const vect & x)
{
	int i, j;
	double som;
	vect res;     // pour le r�sultat du produit
	for (i = 0; i < 3; i++)
	{
		for (j = 0, som = 0; j < 3; j++)
			som += m.mat[i][j] * x.v[j];
		res.v[i] = som;
	}
	return res;
}