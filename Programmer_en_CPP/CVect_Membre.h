#pragma once

#include "CMatrice_Membre.h"
// ********* D�claration de la classe vect **************
class vect
{
	double v[3];      // vecteur � 3 composantes
public:
	vect(double v1 = 0, double v2 = 0, double v3 = 0)   // constructeur
	{
		v[0] = v1; v[1] = v2; v[2] = v3;
	}
	friend vect matrice::prod(const vect &) const;     // prod = fonction amie 
	void affiche() const
	{
		int i;
		for (i = 0; i < 3; i++) cout << v[i] << " ";
		cout << "\n";
	}
};

vect matrice::prod(const vect &x) const
{
	int i, j;
	double som;
	vect res;     // pour le r�sultat du produit
	for (i = 0; i < 3; i++)
	{
		for (j = 0, som = 0; j < 3; j++)
			som += mat[i][j] * x.v[j];
		res.v[i] = som;
	}
	return res;
}
