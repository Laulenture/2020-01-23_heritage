#include "CPoint.h"
#include "CCercle.h"
#include <stdlib.h> 
#include <string>
#include <iostream>



//Assesseurs
int CPoint::getX()
{
	return nX;
}

int CPoint::getY()
{
	return nY;
}

//Mutateurs
void CPoint::setX(int nX)
{
	this->nX = nX;
}

void CPoint::setY(int nY)
{
	this->nY = nY;
}



CPoint::CPoint(int nX, int nY)
{ 
	this->nX = nX;
	this->nY = nY;
	this->cColor = new char[100];
	strcpy_s(this->cColor, 100, "rouge");
	coul = rouge;
}




//Constructeur avec parametres
CPoint::CPoint(int nX, int nY,const char cColor[])
{
	this->nX = nX;
	this->nY = nY;

	this->cColor = new char[strlen(cColor)+1];
	int nTaille = strlen(cColor)+1;
	strcpy_s(this->cColor, nTaille, cColor);
	coul = rouge;
	
}



//Constructeur de copie
CPoint::CPoint(const CPoint & p)
{
	this->nX= p.nX;
    this->nY=p.nY;
	this->cColor = new char[strlen(p.cColor) + 1];
	int nTaille = strlen(p.cColor) + 1;
	strcpy_s(this->cColor, nTaille,p.cColor);
	coul = rouge;
}

CPoint & CPoint::deplacePointRef(const int nX,const int nY)
{
	this->nX = this->nX + nX;
	this->nY = this->nY+nY;
	return *this;

}

CPoint * CPoint::deplacePointAdr(const int nX, const int nY)
{
	this->nX = this->nX + nX;
	this->nY = this->nY + nY;
	return this;

}


CPoint::operator int() const
{
	//conversion d'un point en entier
	return nX;
}

CPoint::~CPoint()
{

	delete cColor;
}


char * CPoint::getColor()
{
	return cColor;
}

void CPoint::setColor(char * cColor)
{
	strcpy_s(this->cColor, strlen(cColor) + 1, cColor);
}


//Affichage d'un point du cercle
void CPoint::affichePointCercle(const CCercle &c)
{
	std::cout << "x : " << this->nX;
	std::cout << "y : " << this->nY;
	std::cout << "Rayon : " << c.nR;
	//on a acc�s aux membres priv�s de c
	std::cout << "Centre x : " << c.pt.nX;
	std::cout << "Centre y : " << c.pt.nY;

}





/*Surcharge operateur*/
CPoint CPoint::operator++() //notation prefixee
{
	nX++, nY++;

	return *this;

}

CPoint CPoint::operator++(int n)//notation postfixe
{
	CPoint p=*this;
	nX++;
	nY++;

	return p;

}

/*CPoint CPoint::operator+(const CPoint &pt)const 
{
	CPoint ptTemp(0, 0, "Red");
	ptTemp.nX = this->nX + pt.nX;
	ptTemp.nY = this->nY + pt.nY;

	return ptTemp;
}*/

CPoint &CPoint:: operator=(const CPoint & pt1)
{
	
	this->nX = pt1.nX;
	this->nY = pt1.nY;

	return *this;
	
}



bool coincide(const CPoint & p, const CPoint & q)
{
	return ((p.nX==q.nX)&&(p.nY==q.nY));
}

void affichePointCercle(const CPoint &pt, const CCercle &c)
{

	//on a acc�s aux membres priv�s de pt et c
	std::cout << "x : " << pt.nX;
	std::cout << "y : " << pt.nY;
	std::cout << "Rayon : " << c.nR;
	std::cout << "Centre x : " << c.pt.nX;
	std::cout << "Centre y : " << c.pt.nY;
}


/*CPoint operator+(const CPoint &pt1, const CPoint &pt2)
{
	CPoint ptRes(0,0,"Red");

	ptRes.nX = pt1.nX + pt2.nX;
	ptRes.nY = pt1.nY + pt2.nY;

	return ptRes;
	//return CPoint(pt1.nX+pt2.nX,pt1.nY+pt1.nY,"") ;
}*/

bool operator==(const CPoint & pt1, const CPoint & pt2)
{
	if ((pt1.nX == pt2.nX) && (pt1.nY == pt2.nY)) {

		return true;
	}

	return false;
}

std::ostream & operator<<(std::ostream & os, const CPoint & pt)
{
	return os << "X:" << pt.nX << "Y:" << pt.nY <<pt.coul<< std::endl;

}

std::istream & operator>>(std::istream & in, CPoint & pt)
{
	in >> pt.nX;
	in >> pt.nY;

	

	return in;
}



