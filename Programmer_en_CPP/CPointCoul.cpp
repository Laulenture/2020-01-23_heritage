#include "CPointCoul.h"
#include <stdlib.h> 
#include <string>
#include <iostream>


//Assesseurs
char * CPointCoul::getCouleur()
{
	return cCouleur;
}

//Mutateurs
void CPointCoul::setCouleur(char* cCouleur)
{
	strcpy_s(this->cCouleur, strlen(cCouleur) + 1, cCouleur);
}


//Constructeurs
CPointCoul::CPointCoul() : CPoint()
{
	
}